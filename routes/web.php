<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    Route::get('/', function () {
        //SEO::setTitle('hello world');
        //SEO::setDescription('This is my page description');
        //SEO::setCanonical('https://boncafe.com.ua');
        SEO::setTitle(trans('index.title'));
        SEO::setDescription(trans('index.description'));
        SEO::setCanonical('https://boncafe.com.ua');
        
        return view('index');
    });

    Route::get('/contact', function () {
        return view('contact');
    });

});