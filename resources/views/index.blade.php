@extends('layouts.app')

@section('content')
    <section class="banner-area">		
        <div class="container">
            <div class="row fullscreen align-items-center justify-content-between">
                <div class="col-lg-12 banner-content">
                    <h1 class="text-white">@lang('index.baner.title')</h1>
                    <p class="text-white">
                    @lang('index.baner.description')
                    </p>

                    <a href="#menu" class="primary-btn text-uppercase">@lang('index.baner.button')</a>
                </div>
            </div>
        </div>					
    </section>
    <!-- End banner Area -->

    <!-- Start home-about Area -->
    <section class="home-about-area section-gap">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 home-about-left">
                    <h1>@lang('index.about.title')</h1>
                    <p>
                    @lang('index.about.description')
                    </p>
                    <a href="#menu" class="primary-btn">@lang('index.about.button')</a>
                </div>
                <div class="col-lg-6 home-about-right">
                    <img class="img-fluid" src="img/new.jpg" alt="бургер boncafe.com.ua">
                </div>
            </div>
        </div>	
    </section>
    <!-- End home-about Area -->			

    <!-- Start menu-area Area -->
    <section class="menu-area section-gap" id="menu">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content pb-70 col-lg-8">
                    <div class="title text-center">
                        <h1 class="mb-10">@lang('index.menu.sectionTitle')</h1>
                        <p>@lang('index.menu.sectionDescription')</p>
                    </div>
                </div>
            </div>	

            <ul class="filter-wrap filters col-lg-12 no-padding">
                <li class="active" data-filter="*">@lang('index.menu.allMenu')</li>
                <li data-filter=".burgers">@lang('index.menu.burgers.menuTitle')</li>
            </ul>
            
            <div class="filters-content">
                <div class="row grid">
                    <div class="col-md-6 all burgers">
                        <div class="single-menu">
                            <div class="title-wrap d-flex justify-content-between">
                                <h4>@lang('index.menu.burgers.burger.title')</h4>
                                <h4 class="price">@lang('index.menu.burgers.burger.price')</h4>
                            </div>  
                            <p>@lang('index.menu.burgers.burger.description')</p>
                            <div class="single-gallery">
                                <img class="img-fluid" src="img/burgers/01.jpg" alt="Класический бургер индюшка/телятина">
                            </div>   									
                        </div>
                    </div>
                    <div class="col-md-6 all burgers">
                        <div class="single-menu">
                            <div class="title-wrap d-flex justify-content-between">
                                <h4>@lang('index.menu.burgers.cheeseburger.title')</h4>
                                <h4 class="price">@lang('index.menu.burgers.cheeseburger.price')</h4>
                            </div>  
                            <p>@lang('index.menu.burgers.cheeseburger.description')</p>
                            <div class="single-gallery">
                                <img class="img-fluid" src="img/burgers/01.jpg" alt="Чизбургер бургер индюшка/телятина">
                            </div>   									
                        </div>
                    </div>  
                    <div class="col-md-6 all burgers">
                        <div class="single-menu">
                            <div class="title-wrap d-flex justify-content-between">
                                <h4>@lang('index.menu.burgers.double.title')</h4>
                                <h4 class="price">@lang('index.menu.burgers.double.price')</h4>
                            </div>  
                            <p>@lang('index.menu.burgers.double.description')</p>
                            <div class="single-gallery">
                                <img class="img-fluid" src="img/burgers/02.jpg" alt="Двойной бургер индюшка/телятина">
                            </div>   									
                        </div>
                    </div>  
                    <div class="col-md-6 all breakfast">
                        <div class="single-menu">
                            <div class="title-wrap d-flex justify-content-between">
                                <h4>Cappuccion</h4>
                                <h4 class="price">$49</h4>
                            </div>			
                            <p>
                                Usage of the Internet is becoming more common due to rapid advance.
                            </p>									
                        </div>					                               
                    </div>                     
                </div>
            </div>
            
        </div>
    </section>
    <!-- End menu-area Area -->			

    <!-- Start review Area -->
    <!--section class="review-area section-gap">
        <div class="container">
            <div class="row">
                <div class="active-review-carusel">
                    <div class="single-review">
                        <img src="img/user.png" alt="">
                        <h4>Hulda Sutton</h4>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>								
                        </div>	
                        <p>
                            “Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker. Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.”
                        </p>
                    </div>
                    <div class="single-review">
                        <img src="img/user.png" alt="">
                        <h4>Hulda Sutton</h4>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>								
                        </div>	
                        <p>
                            “Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker. Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.”
                        </p>
                    </div>	
                    <div class="single-review">
                        <img src="img/user.png" alt="">
                        <h4>Hulda Sutton</h4>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>								
                        </div>	
                        <p>
                            “Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker. Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.”
                        </p>
                    </div>
                    <div class="single-review">
                        <img src="img/user.png" alt="">
                        <h4>Hulda Sutton</h4>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>								
                        </div>	
                        <p>
                            “Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker. Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.”
                        </p>
                    </div>														
                </div>
            </div>
        </div>	
    </section-->
    <!-- End review Area -->		

    <!-- Start contact-page Area -->
    <section class="contact-page-area section-gap"  id="contacts">
    
        <div class="row d-flex justify-content-center">
                <div class="menu-content pb-70 col-lg-8">
                    <div class="title text-center">
                        <h1 class="mb-10">@lang('index.contacts.sectionTitle')</h1>
                        <p>@lang('index.contacts.sectionDescription')</p>
                    </div>
                </div>
            </div>	
        <div class="container">
            <div class="row">
                <div class="col-lg-4 d-flex flex-column address-wrap">
                    <div class="single-contact-address d-flex flex-row">
                        <div class="icon">
                            <span class="lnr lnr-home"></span>
                        </div>
                        <div class="contact-details">
                            <h5>@lang('index.footer.Opening Hours')</h5>
                            <p>
                            <span>@lang('index.footer.Tuesday - Sunday')</span> <span>10:00 - 22:00</span>
                            </p>
                            <p>@lang('index.address')</p>
                        </div>
                    </div>
                    <div class="single-contact-address d-flex flex-row">
                        <div class="icon">
                            <span class="lnr lnr-phone-handset"></span>
                        </div>
                        <div class="contact-details">
                            <h5><a href="tel:+380638122112">+38(063)812-2112</a></h5>
                            <p>@lang('index.footer.Tuesday - Sunday') <span>10:00 - 22:00</span></p>
                        </div>
                    </div>
                    <!--div class="single-contact-address d-flex flex-row">
                        <div class="icon">
                            <span class="lnr lnr-envelope"></span>
                        </div>
                        <div class="contact-details">
                            <h5>support@colorlib.com</h5>
                            <p>Send us your query anytime!</p>
                        </div>
                    </div-->														
                </div>
                <div class="col-lg-8">
                    <form class="form-area contact-form text-right" id="myForm" action="mail.php" method="post">
                        <div class="row">	
                            <div class="col-lg-6 form-group">
                                <input name="name" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" class="common-input mb-20 form-control" required="" type="text">
                            
                                <input name="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" class="common-input mb-20 form-control" required="" type="email">

                                <input name="subject" placeholder="Enter subject" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter subject'" class="common-input mb-20 form-control" required="" type="text">
                            </div>
                            <div class="col-lg-6 form-group">
                                <textarea class="common-textarea form-control" name="message" placeholder="Enter Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Messege'" required=""></textarea>				
                            </div>
                            <div class="col-lg-12">
                                <div class="alert-msg" style="text-align: left;"></div>
                                <button class="genric-btn primary" style="float: right;">Send Message</button>											
                            </div>
                        </div>
                    </form>	
                </div>
            </div>
        </div>	
    </section>
    <!-- End contact-page Area -->
@endsection
