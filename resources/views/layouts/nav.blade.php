<nav id="nav-menu-container">
    <ul class="nav-menu">
        <li><a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(),'/', null) }}">@lang('index.nav.home')</a></li>
        <li><a href="#menu">@lang('index.nav.menu')</a></li>
        <li><a href="#contacts">@lang('index.nav.contacts')</a></li>
        
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
            @if ($localeCode != LaravelLocalization::getCurrentLocale())
            <li>
                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                    {{ $properties['native'] }}
                </a>
            </li>
            @endif
        @endforeach
    </ul>
</nav>