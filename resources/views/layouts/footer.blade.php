<div class="footer-widget-wrap">
					<div class="container">
						<div class="row section-gap">
							<div class="col-lg-3 col-md-3 col-sm-3">
								<div class="single-footer-widget">
									<h4>@lang('index.footer.Opening Hours')</h4>
									<ul class="hr-list">
										<li class="d-flex justify-content-between">
											<span>@lang('index.footer.Monday')</span> <span>@lang('index.footer.Not working')</span>
										</li>
										<li class="d-flex justify-content-between">
											<span>@lang('index.footer.Tuesday - Sunday')</span> <span>10:00 - 22:00</span>
										</li>																			
									</ul>
							</div>
						</div>	
						<div class="col-lg-3 col-md-3 col-sm-3">
							<div class="single-footer-widget">
								<h4>@lang('index.footer.Contact Us')</h4>
								<p class="number">
									<a class="number" href="tel:+380638122112">+38(063)812-2112</a>
								</p>
								<p> @lang('index.address')</p>
							</div>
						</div>		
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<div class="map-responsive">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2538.035706956992!2d30.429988015548734!3d50.49629319194191!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc61c3925839b0896!2sBONCAFE!5e0!3m2!1sru!2sua!4v1558351101073!5m2!1sru!2sua" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>	
							</div>
						</div>					
						</div>					
					</div>					
				</div>
				<div class="footer-bottom-wrap">
					<div class="container">
						<div class="row footer-bottom d-flex justify-content-between align-items-center">
							<p class="col-lg-8 col-mdcol-sm-6 -6 footer-text m-0">
							@lang('index.footer.link', ['link' => '<a href="https://studio-z.com.ua/" target="_blank">Studio-Z</a>'])
							
							<ul class="col-lg-4 col-mdcol-sm-6 -6 social-icons text-right">
	                            <!--li><a href="#"><i class="fa fa-facebook" target="_blank"></i></a></li-->
	                            <li><a href="https://instagram.com/boncafe_" target="_blank"><i class="fa fa-instagram"></i></a></li>           
	                        </ul>
						</div>						
					</div>
				</div>