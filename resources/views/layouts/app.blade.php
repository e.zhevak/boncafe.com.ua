<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-72326812-11"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-72326812-11');
		</script>

        <meta charset="utf-8">
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="favicon.ico">
		<!-- Author Meta -->
		<meta name="author" content="studio-z.com.ua">
		<!-- meta character set -->
		<meta charset="UTF-8">
				
        <meta name="csrf-token" content="{{ csrf_token() }}">
		{!! SEOMeta::generate() !!}
	
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<link rel="stylesheet" href="{{asset('css/linearicons.css')}}">
            <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
            <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
            <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
            <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">

			<link rel="stylesheet" href="{{asset('css/nice-select.css')}}">
            <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
            <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
            <link rel="stylesheet" href="{{asset('css/main.css')}}">
		</head>
		<body>	
            <header id="header">
				<!--div class="header-top">
					<div class="container">
				  		<div class="row justify-content-center">
						      <div id="logo">
						        <a href="/"><img src="img/logo.png" alt="" title="" /></a>
						      </div>
				  		</div>			  					
					</div>
				</div-->
				<div class="container main-menu">
					<div class="row align-items-center justify-content-center d-flex">	
                    
                        @include('layouts.nav')		
				      					      		  
					</div>
				</div>
			</header>

            	@yield('content')

            <!-- start footer Area -->		
			<footer class="footer-area">
                @include('layouts.footer')
			</footer>
			<!-- End footer Area -->	
            <script src="{{asset('js/vendor/jquery-2.2.4.min.js')}}"></script>
            <script src="{{asset('js/popper.min.js')}}"></script>
            <script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
            <script src="{{asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA')}}"></script>
          	<script src="{{asset('js/jquery-ui.js')}}"></script>					
  			<script src="{{asset('js/easing.min.js')}}"></script>			
			<script src="{{asset('js/hoverIntent.js')}}"></script>
			<script src="{{asset('js/superfish.min.js')}}"></script>	
			<script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
			<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>						
			<script src="{{asset('js/jquery.nice-select.min.js')}}"></script>					
			<script src="{{asset('js/owl.carousel.min.js')}}"></script>			
            <script src="{{asset('js/isotope.pkgd.min.js')}}"></script>								
			<script src="{{asset('js/mail-script.js')}}"></script>	
			<script src="{{asset('js/main.js')}}"></script>	
		</body>
	</html>