<?php
return [
    'title' => 'main',
    'description' => 'You can try the best burgers in Kiev at boncafe.com.ua',
    'address' => 'г. Киев ул. ЖК Липинка Сергея Данченко 8',
    'baner' => [
        'title' => 'Tasty and cozy',
        'description' => 'You can try the most delicious burgers in Kiev',
        'button' => 'Our Menu'
    ],
    'about' => [
        'title' => 'About',
        'description' => 'Today the most popular food in the world is burgers, burgers and burgers again. In the same Italy, the establishments specializing in them noticeably pressed even the beloved pizzerias by everyone, and in France burgers are considered a worthy alternative to the classic Parisian sandwich jambon-beurre',
        'button' => 'Our Menu'
    ],
    'nav' => [
        'home' => 'Home',
        'menu' => 'Menu',
        'contacts' =>'Contacts'
    ],
    'footer' => [
        'Opening Hours' => 'Opening Hours',
        'Monday ' => 'Monday',
        'Not working' => 'Not working',
        'Tuesday - Sunday' => 'Tuesday - Sunday',
        'Contact Us' => 'Contact Us',
        'Newsletter' => 'Newsletter',,
        'link' => 'Made by :link'
    ],
    'menu'=> [
        'sectionTitle' => 'Our menu',
        'sectionDescription' => 'We prepare our dishes only from high-quality and fresh products.',
        'allMenu'=> 'All Menu',
        'burgers' => [
            'menuTitle' => 'Burgers',
            'description' => 'Onion, salad, fresh vegetables, сір Cheddar, sauce BBQ',
            'burger' => [
                'title' => 'Burger - Turkey/Beef!',
                'price' => '98 uah/115 uah',
                'description' => 'Onion, salad, fresh vegetables, сір Cheddar, sauce BBQ'
            ],
            'cheeseburger' => [
                'title' => 'Cheeseburger! - Turkey/Beef',
                'price' => '113 uah/130 uah',
                'description' => 'Turkey/beef onion, salad, fresh vegetables, сір Cheddar, sauce BBQ'
            ],
            'double' => [
                'title' => 'Double Burger! - Turkey/Beef',
                'price' => '199 uah/199 uah',
                'description' => 'Turkey/beef onion, salad, fresh vegetables, сір Cheddar, sauce BBQ'
            ]
        ]
    ],
    'contacts' => [
        'sectionTitle' => 'Our contacts',
        'sectionDescription' => 'You can contact us any of these methods.'

    ]
];